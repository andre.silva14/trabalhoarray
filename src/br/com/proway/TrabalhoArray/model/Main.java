package br.com.proway.TrabalhoArray.model;

import javax.swing.*;

public class Main {
    public static void main(String[] args) {
        Caneta[] caneta = new Caneta[2];

        for (int i=0; i < caneta.length; i++){
            caneta[i] = new Caneta();
            caneta[i].id = i+1;
            caneta[i].cor = JOptionPane.showInputDialog(null, "Digite a cor da caneta");
            caneta[i].marca = JOptionPane.showInputDialog(null, "Digite a marca");
        }

        JOptionPane.showMessageDialog(null, "Cor : " + caneta[0].cor +".\n" +"Marca : " +
                caneta[0].marca +".\n" +"Id : " + caneta[0].id);


    }
}
